import i18n from '../i18n'
export default class common {
    static formateTime(date) {
        let formattedDate = '';
        formattedDate = date.substring(0,10);
        return formattedDate
    }

    static formatDateWithoutTime(date, type) {
        if (date !== undefined) {

            const month = [i18n.t("Jan"), i18n.t("Feb"), i18n.t("Mar"), i18n.t("Apr"), i18n.t("May"), i18n.t("Jun"), i18n.t("Jul"), i18n.t("Aug"), i18n.t("Sep"), i18n.t("Oct"), i18n.t("Nov"), i18n.t("Dec")];

            let currentDatetime = new Date(date);

            let year = '';
            let c = "-";
            if (type === 'year') {
                year = " " + currentDatetime.getFullYear();
                c = " ";
            }

            return currentDatetime.getDate() +
                c +
                (month[currentDatetime.getMonth()])
            + year
        }
    }

    static formatDate(date, type) {
        if (date !== undefined) {
            let currentDatetime = new Date(date);

            let hours = "";
            if (type !== 'no time') {
                let pmOrAm = i18n.t('AM');
                let hour = date.substring(11,16);
                if (date.substring(11,13) > 12) {
                    pmOrAm = i18n.t('PM');
                    hour = (parseInt(date.substring(11,13)) - 12) + date.substring(13, 16)
                }

                hours = "  " + hour + "  " + pmOrAm;
            }

            return currentDatetime.getDate() +
                "-" +
                (currentDatetime.getMonth() + 1) +
                "-" +
                currentDatetime.getFullYear() + hours;

        }
    }

    static formatTime(date) {
        if (date !== undefined) {

            let pmOrAm = i18n.t('AM');
            let hour = date.substring(11,16);
            if (date.substring(11,13) > 12) {
                pmOrAm = i18n.t('PM');
                hour = (parseInt(date.substring(11,13)) - 12) + date.substring(13, 16)
            }
            return hour + "  " + pmOrAm
        }
    }

    static getTime(date) {
        let formattedDate = '';
        formattedDate = date.substring(12,16);
        return this.tConvert(formattedDate)
    }

    static tConvert (time) {
        // Check correct time format and split into components
        time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-\\\\5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice (1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
            time[3] = ''
        }
        return time.join (''); // return adjusted time or original string
    }

     static youtube_parser(url){
        let regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        let match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }


}
