import axios from "axios";

export default {
    state: {
        token: "",
        admin: null,
        name:'',
        email:'',
        interests:[],
        vip:''
    },
    mutations: {
        SET_USER_TOKEN(state, data) {
            state.token = data.token;
            state.name = data.name;
            state.email = data.email;
            state.vip = data.vip;
            },
        SET_USER_INFO(state, data){
            state.token = data.token;
            state.name = data.name;
            state.email = data.email;
        },
        UPDATE_USER_INFO(state, data){
            state.name = data.name;
            state.email = data.email;
        },
        logout(state) {
            state.status.token = "";
        },
        SET_DATA(state , data){
            state.interests = data;
        }
    },
    actions: {
        setUserToken({ commit }, token) {
            commit("SET_USER_TOKEN", token);
        },
        setUserInfo({ commit } , data){
            commit("SET_USER_INFO", data);
        },
        updateUserInfo({ commit } , data){
            commit("UPDATE_USER_INFO", data);
        },
        setBook({ commit } , data){
            commit("SET_DATA", data);
        },
        logout({ commit }) {
            return new Promise((resolve, reject) => {
                commit("LOGOUT");
                localStorage.removeItem("userToken");
                delete axios.defaults.headers.common["Authorization"];
                resolve();
                location.reload(true);
            });
        }
    },
    getters:{
        isLoggedIn: state => !!state.token,
    }
};
