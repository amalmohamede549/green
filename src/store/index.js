import Vue from 'vue'
import Vuex from 'vuex'

import modules from "./modules";

import VuexPersist from 'vuex-persist'

Vue.use(Vuex)
const vuexPersist = new VuexPersist({
    key: 'my-app',
    storage: window.localStorage
})
// plugins: [vuexPersist.plugin]

const store = new Vuex.Store({
    modules,

})

export default store
