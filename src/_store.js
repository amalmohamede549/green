import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { BASE_API } from "./utils/server";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        status: "",
        token: localStorage.getItem("userToken") || "",
        user: {},
        email:'',
        password:'',
        vip:''
    },
    mutations: {
        auth_request(state) {
            state.status = "loading";
        },
        auth_success(state, token, user ,email , vip) {
            state.status = "success";
            state.token = token;
            state.user = user;
            state.email = email;
            state.vip = vip;
        },
        auth_error(state) {
            state.status = "error";
        },
        logout(state) {
            state.status = "";
            state.token = "";
        }
    },
    actions: {
        login({ commit }, user) {
            return new Promise((resolve, reject) => {
                commit("auth_request");
                axios({ url: `${BASE_API}/login`, data: user, method: "POST" })
                    .then(resp => {
                        const token = resp.data.token;
                        const user = resp.data.name;
                        localStorage.setItem("userToken", token);
                        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
                        commit("auth_success", token, user);
                        resolve(resp);
                    })
                    .catch(err => {
                        commit("auth_error");
                        localStorage.removeItem("userToken");
                        reject(err);
                    });
            });
        },
        register({ commit }, user) {
            return new Promise((resolve, reject) => {
                commit("auth_request");
                axios({ url: `${BASE_API}/register`, data: user, method: "POST" })
                    .then(resp => {
                        const token = resp.data.token;
                        const user = resp.data.name;
                        const email = resp.data.email;
                        const status = resp.data.status;
                        const vip = resp.data.vip;

                        localStorage.setItem("userToken", token);
                        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
                        commit("auth_success", token, user , email , status , vip);
                        resolve(resp);
                    })
                    .catch(err => {
                        commit("auth_error");
                        localStorage.removeItem("userToken");
                        reject(err);
                    });
            });
        },
        updatePassword({ commit }, user) {
            return new Promise((resolve, reject) => {
                commit("auth_request");
                axios({ url: `${BASE_API}/change_password`, data: user, method: "POST" })
                    .then(resp => {
                        const token = resp.data.token;
                        const user = resp.data.user;
                        localStorage.setItem("userToken", token);
                        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
                        commit("auth_success", token, user);
                        resolve(resp);
                        console.log('resp' , resp);
                    })
                    .catch(err => {
                        commit("auth_error");
                        localStorage.removeItem("userToken");
                        reject(err);
                    });
            });
        },
        logout({ commit }) {
            return new Promise((resolve, reject) => {
                commit("logout");
                localStorage.removeItem("userToken");
                delete axios.defaults.headers.common["Authorization"];
                resolve();
            });
        }
    },
    getters: {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status
    }
})
