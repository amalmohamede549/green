import Vue from "vue";
import { extend, localize } from "vee-validate";
import { required, email, min } from "vee-validate/dist/rules";
// import ar from "./vee-validate-ar";
import ar from "vee-validate/dist/locale/ar";
import en from "vee-validate/dist/locale/en";

// Install required rule.
extend("required", required);

// Install email rule.
extend("email", email);

// Install min rule.
extend("min", min);

// Install English and Arabic localizations.
localize({
    ar: {
        messages: ar.messages,
        names: {
            name:'الاسم',
            email: "البريد الاليكتروني",
            password: "كلمة السر",
            duplicatePassword: "كلمة السر",
            notification:' ',
            message:'الرساله',
            oldpassword:'كلمه المرور القديمه' ,
            newpassword:'كلمه المرور الجديده'

        }
    }
});
localize('ar');

let LOCALE = "ar";

// A simple get/set interface to manage our locale in components.
// This is not reactive, so don't create any computed properties/watchers off it.
Object.defineProperty(Vue.prototype, "locale", {
    get() {
        return LOCALE;
    },
    set(val) {
        LOCALE = val;
        localize(val);
    }
});
