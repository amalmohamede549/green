import Vue from 'vue'
import axios from 'axios'
import i18n from '../i18n'
import store from '../store/index'

let apiBaseUrl = process.env.VUE_APP_BASE_URL

// window.api_root = process.env.VUE_APP_BASE_URL;




// Vue.prototype.$http = axios


// let lang = localStorage.getItem('lang')

//no auth
Vue.prototype.$http = axios.create({
    baseURL: apiBaseUrl,
});

//auth
Vue.prototype.$http_auth = axios.create({
    baseURL: apiBaseUrl,
    headers: {
        Authorization: "Bearer " + store.state.user.token
    }});
