import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/src/stylus/app.styl'

let rtl = localStorage.getItem('rtl')

Vue.use(Vuetify, {
  iconfont: 'md',
  rtl: true,
  theme: {
    primary: '#1E7145',
    secondary: '#F5F5F5',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107',
    grey: '#616161'
  },
})
