import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import login from './views/login.vue'
import register from './views/register.vue'
import homeAfterLogin from './views/home-after-login.vue'
import latestRecommendations from './views/latest-recommendations-view'
import singleRecommendation from '../src/components/latest-recommendations-page/single-recommendation'
import sections from './views/sections-view'
import singleSection from '../src/components/sections-page/single-section'
import blogs from './views/blog-view'
import singleBlog from '../src/components/blog-page/single-blog'
import favorites from './views/favorites-view'
import settings from './views/settings-view'
import contactUs from './views/contact-us-view'
import experimental from './views/experimental-view'
import newlyAdded from './views/newly-added-view'
import privacyPolicy from './views/privacy-view'
import packages from './views/packages-view'
import search from '../src/components/pages/search'
import pageTemplate from '../src/components/pages/page-template'
import store from "./store";

import library from '../src/components/favorites-component/library-component'
import bookmarks from '../src/components/favorites-component/bookmarks-component'



Vue.use(Router);

export  const router = new Router({
    mode: 'history',
    scrollBehavior (to, savedPosition) {
        // if (savedPosition) {
        //     return savedPosition
        // } else {
        //     return { x: 0, y: 0 }
        // }
        return { x: 0, y: 0 }
    },
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
             meta: {
                  requiresAuth: false,
                 title: 'Home Page'

             },
        },
        {
            path: '/login',
            name: 'login',
            component: login,
             meta: {
                  requiresAuth: false,
                 title: 'login'
                },
        },
        {
            path: '/register',
            name: 'register',
            component: register,
             meta: {
                  requiresAuth: false,
                 title: 'register'
                },
        },
        {
            path: '/list',
            name: 'recommendations',
            component: latestRecommendations,
             meta: {
                  requiresAuth: false,
                 title: 'list'

             },
        },
        {
            path: '/list/:id',
            name: 'singleRecommendation',
            component: singleRecommendation,
             meta: {
                  requiresAuth: false,
                 title: 'list'

             },
        },
        {
            path: '/sections/:id',
            name: 'sections',
            component: sections,
             meta: {
                  requiresAuth: false,
                 title: 'sections'

             },
        },
        {
            path: '/summaries/:id',
            name: 'singleSection',
            component: singleSection,
             meta: {
                  requiresAuth: false,
                 title: 'summaries',
                 subtitle: 'asd'

             },
        },
        {
            path: '/book',
            name: 'blogs',
            component: blogs,
             meta: {
                  requiresAuth: false,
                 title: 'book'

             },
        },
        {
            path: '/book/:id',
            name: 'singleBlog',
            component: singleBlog,
             meta: {
                  requiresAuth: false,
                     title: 'book',

             },
        },
        {
            path: '/library',
            name: 'library',
            component: library,
             meta: {
                  requiresAuth: true,
                 title: 'library'

             },
        },
        {
            path: '/favorites',
            name: 'favorites',
            component: favorites,
            meta: {
                requiresAuth: true,
                title: 'favorites'

            },
        },
        {
            path: '/bookmarks',
            name: 'bookmarks',
            component: bookmarks,
            meta: {
                requiresAuth: true,
                title: 'bookmarks'

            },
        },
        {
            path: '/settings',
            name: 'settings',
            component: settings,
             meta: {
                  requiresAuth: true,
                 title: 'settings'

             },
        },
        {
            path: '/contact-us',
            name: 'contactUs',
            component: contactUs,
             meta: {
                  requiresAuth: false,
                 title: 'contactUs'

             },
        },
        {
            path: '/experimental',
            name: 'experimental',
            component: experimental,
             meta: {
                  requiresAuth: true,
                 title: 'experimental'

             },
        },
        {
            path: '/books/:slug',
            name: 'newlyAdded',
            component: newlyAdded,
             meta: {
                  requiresAuth: false,
                 title: 'books'

             },
        },
        {
            path: '/privacy-policy',
            name: 'privacyPolicy',
            component: privacyPolicy,
             meta: {
                  requiresAuth: false,
                 title: 'privacy policy'


             },
        },
        {
            path: '/search',
            name: 'search',
            component: search,
             meta: {
                  requiresAuth: false,
                 title: 'Search'

             },
        },
        {
            path: '/template',
            name: 'pageTemplate',
            component: pageTemplate,
             meta: {
                  requiresAuth: false,
                 title: 'Page Template'

             },
        },
        {
            path: '/packages',
            name: 'packages',
            component: packages,
             meta: {
                  requiresAuth: true,
                 title: 'Package'

             },
        },


    ]
})

//
// router.beforeEach((to, from, next) => {
//     // redirect to login page if not logged in and trying to access a restricted page
//     if (!store.state.user.userToken && to.path !== '/login') {
//         return next('/login');
//     }
//     if (store.state.user.userToken && to.path === '/login') {
//         return next('/');
//     }
//
//     next();
// })
//
//
// function getRoutesList(routes, pre) {
//     return routes.reduce((array, route) => {
//         const path = `${pre}${route.path}`;
//
//         if (route.path !== '*') {
//             array.push(path);
//         }
//
//         if (route.children) {
//             array.push(...getRoutesList(route.children, `${path}/`));
//         }
//
//         return array;
//     }, []);
// }
//
// function getRoutesXML() {
//     const list = getRoutesList(router.options.routes, 'https://a5dr.com')
//         .map(route => `<url><loc>${route}</loc></url>`)
//         .join('\r\n');
//     return `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
//     ${list}
//   </urlset>`;
// }
//
// getRoutesXML();
//
// console.log(getRoutesXML)
// export default router
