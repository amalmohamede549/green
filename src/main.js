import Vue from 'vue'
import i18n from './i18n'
import './plugins/axios'
import './plugins/vuetify'
import './plugins/GMaps'
import NProgress from 'nprogress'
import App from './App.vue'
import {router} from './router'
import store from './store'
import './plugins/vee-validate'
import Meta from 'vue-meta'





import loader from './components/loader/loader'

//plugins
Vue.use(NProgress);


Vue.config.productionTip = false;

const token = localStorage.getItem("userToken");
if (token) {
    Vue.prototype.$http.defaults.headers.common["Authorization"] = token;
}
Vue.use(Meta, {
    keyName: "metaInfo", // the component option name that vue-meta looks for meta info on.
    attribute: "data-vue-meta", // the attribute name vue-meta adds to the tags it observes
    ssrAttribute: "data-vue-meta-server-rendered", // the attribute name that lets vue-meta know that meta info has already been server-rendered
    tagIDKeyName: "vmid" // th
});

console.log('store.getters.isLoggedIn');
router.beforeResolve((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            next();
            return;
        }
        next("/login");
    } else {
        next();
    }
});
router.beforeResolve((to, from, next) => {
    if (to.path) {
        NProgress.start()
    }
    next()
});
router.beforeEach((to, from, next) => {
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));
    if(!nearestWithMeta) return next();
    nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');
        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });
        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
    }).forEach(tag =>  {
        document.head.appendChild(tag)
    });
    next();
});


Vue.component('loaderComponent' , loader);


// lsSync.syncData();




router.afterEach(() => {
  NProgress.done()
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
